var startModule = (function start() {
    var renderOnClientSide = function () {
       var payload = getClientPayload();
       new Chartist[payload.type]("#client_content", payload.data, payload.options);
    },
    renderOnServerSide = function () {
        sendDataToController("/chartist/render", getServerPayload(), function onSuccess(data){
            $(".ct-chart").remove();
             $("#server_content").prepend($(data));
        });
    },
    getClientPayload = function() {
        return {
            "type":  "Bar",
            "options": {"width": 600, "height": 200},
            "data": {
                "labels": ["Spint1","Sprint2","Sprint3","Sprint4","Sprint5"],
                "series": [
                    [32, 34, 45, 25, 19],
                    [15, 20, 35, 10, 9]
                ]
            }
        }
    },
    getServerPayload = function() {
            return {
                "type":  "Line",
                "options": {"width": 600, "height": 200,  "showArea": true},
                "data": {
                    "labels": ["Spint1","Sprint2","Sprint3","Sprint4","Sprint5"],
                    "series": [
                        [32, 34, 45, 25, 19],
                        [15, 20, 35, 10, 9]
                    ]
                }
            }
    },
    sendDataToController = function (link, data, onSuccessCallback, onErrorCallback) {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: link,
            success: function(data) {
                console.log('success' + data);
                onSuccessCallback(data);
            },
            error: function (data) {
                console.log('error:' + data);
                onErrorCallback(data);
            }
        });
    };
    return {
        renderOnClientSide: renderOnClientSide,
        renderOnServerSide:renderOnServerSide
    };
})();
