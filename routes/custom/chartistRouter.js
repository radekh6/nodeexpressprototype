const generate = require('node-chartist');
var express = require('express');
var router = express.Router();

router.post('/render', function (req, res, next) {
    console.log("BODY: " + JSON.stringify(req.body));
    generate(req.body.type, req.body.options, req.body.data).then(function (svg) {
        res.send(svg);
    }, function (what) {
        res.status(400).send("Not found: " + what);
    })
});

module.exports = router;

